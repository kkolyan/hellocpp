//
// Created by kkolyan on 2021-01-16.
//

#ifndef HELLOCPP_HELLOCPP_H
#define HELLOCPP_HELLOCPP_H

#include <iostream>
#include "GridVector.h"

class HelloCpp {
public:
    static void hello();
    static void simple();
    static void helloConst();
    static void helloPtr();
    static void helloConstPtr();
    static void helloPtrConst();
    static void helloRef();
    static void helloLocalPtr();
    static void helloConstRef();
    static void helloConstRefToMutable();
};


#endif //HELLOCPP_HELLOCPP_H
