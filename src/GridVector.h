//
// Created by kkolyan on 2021-01-15.
//

#ifndef HELLOECS_GRIDVECTOR_H
#define HELLOECS_GRIDVECTOR_H


#include <ostream>

class GridVector {
public:
    int x;
    int y;

    GridVector(int x, int y);

    friend std::ostream &operator<<(std::ostream &os, const GridVector &vector);
};


#endif //HELLOECS_GRIDVECTOR_H
