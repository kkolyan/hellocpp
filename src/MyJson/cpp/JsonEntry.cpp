//
// Created by kkolyan on 2021-01-16.
//

#include "../h/JsonEntry.h"

using namespace MyJson;

JsonEntry::JsonEntry(const std::string &key, MyJson::JsonNode *value) : key(key), value(value) {

}

std::ostream &JsonEntry::format(std::ostream &os) {
    os << key;
    os << ": ";
    value->format(os);
    return os;
}
