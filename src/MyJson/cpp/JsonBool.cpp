//
// Created by kkolyan on 2021-01-19.
//

#include "../h/JsonBool.h"

using namespace MyJson;

JsonBool::JsonBool(bool value) : value(value) {

}

std::ostream &JsonBool::format(std::ostream &os) {
    if (value) {
        return os << "true";
    }
    return os << "false";
}
