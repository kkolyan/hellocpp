//
// Created by kkolyan on 2021-01-19.
//

#include "../h/JsonNumberLong.h"

using namespace MyJson;

JsonNumberLong::JsonNumberLong(long value) : value(value) {

}

std::ostream &JsonNumberLong::format(std::ostream &os) {
    return os << value;
}
