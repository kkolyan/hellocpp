//
// Created by kkolyan on 2021-01-19.
//

#include "../h/JsonNull.h"

std::ostream &MyJson::JsonNull::format(std::ostream &os) {
    return os << "null";
}
