//
// Created by kkolyan on 2021-01-16.
//

#include "../h/JsonArray.h"
#include "../hpp/joinIterable.hpp"

using namespace MyJson;

JsonArray::~JsonArray() {
    for (JsonNode *element: elements) {
        delete element;
    }
    elements.clear();
}

std::ostream &JsonArray::format(std::ostream &os) {
    os << "[";
    MyJson::joinIterable(os, elements, ", ");
    os << "]";
    return os;
}
