//
// Created by kkolyan on 2021-01-16.
//

#include "../h/JsonObject.h"
#include "../hpp/joinIterable.hpp"

using namespace MyJson;
using namespace MyJson;

MyJson::JsonObject::~JsonObject() {
    for (JsonEntry *entry: entries) {
        delete entry;
    }
    entries.clear();
    entriesByName.clear();
}

std::ostream &JsonObject::format(std::ostream &os) {
    os << "{";
    MyJson::joinIterable(os, entries, ", ");
    os << "}";
    return os;
}
