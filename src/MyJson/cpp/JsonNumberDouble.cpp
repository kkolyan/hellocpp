//
// Created by kkolyan on 2021-01-19.
//

#include "../h/JsonNumberDouble.h"

using namespace MyJson;

JsonNumberDouble::JsonNumberDouble(double value) : value(value) {}

std::ostream &JsonNumberDouble::format(std::ostream &os) {
    os << std::dec << value;
    return os;
}
