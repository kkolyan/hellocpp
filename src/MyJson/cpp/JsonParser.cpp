//
// Created by kkolyan on 2021-01-16.
//

#include "../h/JsonParser.h"
#include <fstream>

using namespace MyJson;


JsonParser::JsonParser(const std::string& fileName) : in(fileName) {
    if (!in.is_open()) {
        throw std::invalid_argument("cannot open file: " + fileName);
    }
}

JsonNode *JsonParser::parse() {
    nextToken(1);
    if (current == "[") {
        return parseArray();
    }
    if (current == "{") {
        return parseObject();
    }
    if (current == "\"") {
        return new JsonText(parseText());
    }
    rollback();
    nextToken(4);
    if (current == "true") {
        return new JsonBool(true);
    }
    if (current == "null") {
        return JsonNull::getInstance();
    }
    rollback();

    nextToken(5);
    if (current == "false") {
        return new JsonBool(false);
    }
    rollback();

    return this->parseNumber();
}

JsonArray *JsonParser::parseArray() {
    expect("[");
    JsonArray *array = new JsonArray();
    int n = 0;
    while (true) {
        nextToken(1);
        if (current == "]") {
            break;
        }
        if (n++ > 0) {
            expect(",");
        } else {
            // other than "," and "]" are the part of next element
            rollback();
        }
        JsonNode *node = parse();
        array->elements.push_back(node);
    }
    return array;
}

JsonObject *JsonParser::parseObject() {
    expect("{");
    JsonObject *object = new JsonObject();
    int n = 0;
    while (true) {
        nextToken(1);
        if (current == "}") {
            break;
        }
        if (n++ > 0) {
            expect(",");
        } else {
            // other than "," and "}" are the part of next element
            rollback();
        }
        nextToken(1);
        std::string key = parseText();

        nextToken(1);
        expect(":");

        JsonNode *value = parse();

        object->entries.push_back(new JsonEntry(key, value));
    }
    return object;
}

std::string JsonParser::parseText() {
    expect("\"");
    std::string text;
    while (true) {
        nextToken(1);
        if (current == "\"") {
            break;
        }
        text.append(current);
    }
    return text;
}

void JsonParser::nextToken(int length) {
    current = "";
    while (true) {
        if (current.length() >= length) {
            break;
        }
        char c = nextChar();
        if (c < 0) {
            break;
        }

        if (std::isspace(c)) {
            continue;
        }
        current.append(1, c);
    }
}

char JsonParser::nextChar() {
    if (buffer.empty()) {
        if (eof) {
            return -1;
        }
        char buf[1024];
        if (!in.read(buf, 1024)) {
            eof = true;
        }
        std::streamsize n = in.gcount();
        buffer.append(buf, n);
    }
    char c = buffer.front();
    buffer.erase(0, 1);
    return c;
}

void JsonParser::expect(std::string s) {
    if (current != s) {
        throw new std::invalid_argument("unexpected token: " + current);
    }
}

void JsonParser::rollback() {
    buffer = current + buffer;
    current = "";
}

JsonNumber *JsonParser::parseNumber() {
    // skip whitespace and get try sign
    nextToken(1);
    int sign = 1;
    if (current == "-") {
        sign = -1;
    } else {
        rollback();
    }
    long number = 0;
    bool fractionExpected = false;

    while (true) {
        char c = nextChar();
        if (c < 0) {
            break;
        }
        if (c == '.') {
            fractionExpected = true;
            break;
        }
        if (!std::isdigit(c)) {
            buffer.insert(0, &c, 1);
            break;
        }
        number *= 10;
        number += (c - '0');
    }
    if (!fractionExpected) {
        return new JsonNumberLong(sign * number);
    }

    int fractionDigits = 0;
    long fractionValue = 0;

    while (true) {
        char c = nextChar();
        if (c < 0) {
            break;
        }
        if (!std::isdigit(c)) {
            buffer.insert(0, &c, 1);
            break;
        }
        fractionValue *= 10;
        fractionValue += (c - '0');
        fractionDigits++;
    }

    return new JsonNumberDouble(sign * number + 1.0 * fractionValue / fractionDigits);
}
