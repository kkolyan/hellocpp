//
// Created by kkolyan on 2021-01-16.
//

#include "../h/JsonText.h"

using namespace MyJson;

JsonText::JsonText(const std::string &text) : text(text) {

}

std::ostream &JsonText::format(std::ostream &os) {
    return os << "\"" << text << "\"";
}
