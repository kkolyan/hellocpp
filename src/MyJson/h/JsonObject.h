//
// Created by kkolyan on 2021-01-16.
//

#ifndef HELLOCPP_JSONOBJECT_H
#define HELLOCPP_JSONOBJECT_H

#include <unordered_map>
#include <string>
#include <vector>
#include <ostream>
#include "JsonNode.h"
#include "JsonEntry.h"

namespace MyJson {
    class JsonObject : public JsonNode {
    public:
        std::vector<JsonEntry *> entries;
        std::unordered_map<std::string, JsonEntry *> entriesByName;

        virtual ~JsonObject();

        std::ostream &format(std::ostream &os) override;
    };


}

#endif //HELLOCPP_JSONOBJECT_H
