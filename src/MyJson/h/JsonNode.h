//
// Created by kkolyan on 2021-01-16.
//

#ifndef HELLOCPP_JSONNODE_H
#define HELLOCPP_JSONNODE_H

#include <ostream>

namespace MyJson {
    class JsonNode {

    public:
        virtual ~JsonNode();

        virtual std::ostream &format(std::ostream &os);
    };
}

#endif //HELLOCPP_JSONNODE_H
