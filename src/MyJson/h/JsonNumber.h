//
// Created by kkolyan on 2021-01-16.
//

#ifndef HELLOCPP_JSONNUMBER_H
#define HELLOCPP_JSONNUMBER_H

#include <string>
#include <ostream>
#include "JsonNode.h"

namespace MyJson {
    class JsonNumber : public JsonNode {
    public:
        virtual std::ostream &format(std::ostream &os) override;
    };
}


#endif //HELLOCPP_JSONNUMBER_H
