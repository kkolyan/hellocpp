//
// Created by kkolyan on 2021-01-16.
//

#ifndef HELLOCPP_JSONARRAY_H
#define HELLOCPP_JSONARRAY_H

#include <vector>
#include <ostream>
#include "JsonNode.h"

namespace MyJson {
    class JsonArray : public JsonNode {
    public:
        virtual ~JsonArray();

        std::vector<JsonNode *> elements;

        std::ostream &format(std::ostream &os) override;
    };
}

#endif //HELLOCPP_JSONARRAY_H
