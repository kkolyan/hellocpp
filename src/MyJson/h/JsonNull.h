//
// Created by kkolyan on 2021-01-19.
//

#ifndef HELLOCPP_JSONNULL_H
#define HELLOCPP_JSONNULL_H


#include "JsonNode.h"

namespace MyJson {
    class JsonNull : public JsonNode {

    public:
        std::ostream &format(std::ostream &os) override;

        static JsonNull *getInstance() {
            static JsonNull instance;
            return &instance;
        }

    private:
        JsonNull() {}

    public:
        JsonNull(JsonNull const &) = delete;

        void operator=(JsonNull const &) = delete;
    };
}


#endif //HELLOCPP_JSONNULL_H
