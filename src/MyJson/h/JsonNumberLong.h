//
// Created by kkolyan on 2021-01-19.
//

#ifndef HELLOCPP_JSONNUMBERLONG_H
#define HELLOCPP_JSONNUMBERLONG_H

#include <ostream>
#include "JsonNumber.h"

namespace MyJson {

    class JsonNumberLong : public JsonNumber {
    public:
        long value;

        JsonNumberLong(long value);

        std::ostream &format(std::ostream &os) override;
    };
}

#endif //HELLOCPP_JSONNUMBERLONG_H
