//
// Created by kkolyan on 2021-01-16.
//

#ifndef HELLOCPP_JSONENTRY_H
#define HELLOCPP_JSONENTRY_H

#include <string>
#include <ostream>
#include "JsonNode.h"

namespace MyJson {
    class JsonEntry {
    public:
        std::string key;
        JsonNode *value;

        JsonEntry(const std::string &key, JsonNode *value);

        std::ostream &format(std::ostream &os);
    };
}

#endif //HELLOCPP_JSONENTRY_H
