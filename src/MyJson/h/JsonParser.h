//
// Created by kkolyan on 2021-01-16.
//

#ifndef HELLOCPP_JSONPARSER_H
#define HELLOCPP_JSONPARSER_H

#include <iostream>
#include <fstream>
#include "JsonNumber.h"
#include "JsonText.h"
#include "JsonObject.h"
#include "JsonArray.h"
#include "JsonNode.h"
#include "JsonBool.h"
#include "JsonNumberLong.h"
#include "JsonNumberDouble.h"
#include "JsonNull.h"

namespace MyJson {
    class JsonParser {
    public:
        explicit JsonParser(const std::string &fileName);

        JsonNode *parse();

    private:
        std::ifstream in;
        bool eof = false;
        std::string buffer;
        std::string current;

        void nextToken(int length);

        JsonArray *parseArray();

        JsonObject *parseObject();

        std::string parseText();

        void expect(std::string s);

        void rollback();

        JsonNumber *parseNumber();

        char nextChar();
    };
}


#endif //HELLOCPP_JSONPARSER_H
