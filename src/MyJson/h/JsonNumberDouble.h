//
// Created by kkolyan on 2021-01-19.
//

#ifndef HELLOCPP_JSONNUMBERDOUBLE_H
#define HELLOCPP_JSONNUMBERDOUBLE_H

#include <ostream>
#include "JsonNumber.h"

namespace MyJson {
    class JsonNumberDouble : public JsonNumber {
    public:
        double value;

        JsonNumberDouble(double value);

        std::ostream &format(std::ostream &os) override;
    };
}


#endif //HELLOCPP_JSONNUMBERDOUBLE_H
