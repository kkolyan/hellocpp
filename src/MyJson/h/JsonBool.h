//
// Created by kkolyan on 2021-01-19.
//

#ifndef HELLOCPP_JSONBOOL_H
#define HELLOCPP_JSONBOOL_H

#include <ostream>
#include "JsonNode.h"

namespace MyJson {
    class JsonBool : public JsonNode {
    public:
        bool value;

        JsonBool(bool value);

        std::ostream &format(std::ostream &os) override;
    };
}


#endif //HELLOCPP_JSONBOOL_H
