//
// Created by kkolyan on 2021-01-16.
//

#ifndef HELLOCPP_JSONTEXT_H
#define HELLOCPP_JSONTEXT_H


#include <string>
#include <ostream>
#include "JsonNode.h"

namespace MyJson {
    class JsonText : public JsonNode {
    public:
        const std::string text;

        JsonText(const std::string &text);

        std::ostream &format(std::ostream &os) override;
    };
}


#endif //HELLOCPP_JSONTEXT_H
