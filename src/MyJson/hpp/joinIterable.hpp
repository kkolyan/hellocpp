#ifndef HELLOCPP_JOINITERABLE_H
#define HELLOCPP_JOINITERABLE_H

#include <iterator>
#include <utility>
#include <ostream>


namespace MyJson {
    template<class T>
    inline std::ostream &joinIterable(std::ostream &os, const T &items, const std::string &delimiter) {
        std::string s;
        for (const auto &item : items) {
            os << s;
            item->format(os);
            s = delimiter;
        }
        return os;
    }
}

#endif //HELLOCPP_JOINITERABLE_H