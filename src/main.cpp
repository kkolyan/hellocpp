#pragma clang diagnostic push
#pragma ide diagnostic ignored "modernize-use-auto"

#include <iostream>
#include <fstream>
#include "HelloCpp.h"
#include "MyJson/h/JsonParser.h"
using namespace MyJson;

int main() {
    JsonParser parser("c:/dev/cpp/hellocpp/src/MyJson/test.json");
    JsonNode *pNode = parser.parse();

    pNode->format(std::cout);
//    HelloCpp::hello();

    std::cout << "Hello, World!" << std::endl;
    return 0;
}

#pragma clang diagnostic pop