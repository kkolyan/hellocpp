//
// Created by kkolyan on 2021-01-16.
//

#include "HelloCpp.h"


void HelloCpp::hello() {
    simple();
    helloConst();
    helloPtr();
    helloLocalPtr();
    helloConstPtr();
    helloPtrConst();
    helloRef();
    helloConstRef();
    helloConstRefToMutable();
}


void HelloCpp::helloConstRefToMutable() {
    GridVector v(42, 1);
    const GridVector &r = v;
//    r.y = 5;//cannot
//    std::cout << v << std::endl;
}

void HelloCpp::helloConstRef() {
    const GridVector v(42, 1);
    const GridVector &r = v;
//    r.y = 5; // cannot
    //std::cout << v << std::endl;
}

void HelloCpp::helloRef() {
    GridVector v(42, 1);
    GridVector v2(42, 7);
    GridVector &r = v;
    r.y = 5;
    //&r = v2; // is not allowed to change a reference itself
    std::cout << v << std::endl;
}

void HelloCpp::helloPtrConst() {
    GridVector *const v = new GridVector(42, 17);
    std::cout << *v << std::endl;
    v->y = 8;
    std::cout << *v << std::endl;
    delete v;
    // v = new GridVector(2, 2);// cannot
//    std::cout << *v << std::endl;
//    delete v;
}

void HelloCpp::helloConstPtr() {
    const GridVector *v = new GridVector(42, 17);
    std::cout << *v << std::endl;
    // v->y = 8;// cannot
//    std::cout << *v << std::endl;
    delete v;
    v = new GridVector(2, 2);
    std::cout << *v << std::endl;
    delete v;
}

void HelloCpp::helloLocalPtr() {
    GridVector v(42, 1);
    GridVector *r = &v;
    r->y = 7;
    std::cout << v << std::endl;
}

void HelloCpp::helloPtr() {
    GridVector *v = new GridVector(42, 17);
    std::cout << *v << std::endl;
    v->y = 8;
    std::cout << *v << std::endl;
    delete v;
    v = new GridVector(2, 2);
    std::cout << *v << std::endl;
    delete v;
}

void HelloCpp::helloConst() {
    const GridVector v(42, 17);
    std::cout << v << std::endl;
    //v.y = 7; //cannot
}

void HelloCpp::simple() {
    GridVector v(42, 1);
    std::cout << v << std::endl;
    v.y = 5;
    std::cout << v << std::endl;
}