//
// Created by kkolyan on 2021-01-15.
//

#include "GridVector.h"

GridVector::GridVector(int x, int y) : x(x), y(y) {}

std::ostream &operator<<(std::ostream &os, const GridVector &vector) {
    os << "x: " << vector.x << " y: " << vector.y;
    return os;
}
